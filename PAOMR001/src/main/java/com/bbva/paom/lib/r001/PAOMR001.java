package com.bbva.paom.lib.r001;

import java.util.List;
import java.util.Map;

public interface PAOMR001 {

	List<Map<String, Object>> executeGetVersionInformation();

}
