package com.bbva.paom.lib.r001.impl;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.apx.exception.db.NoResultException;

public class PAOMR001Impl extends PAOMR001Abstract {

	private static final Logger LOGGER = LoggerFactory.getLogger("PAOMR001Impl");
	private static final String ERROR_GET_BASIC_DATA = "PAOM00000001";
	private static final String TRACE_PAOMR001_IMPL = " [PAOM][PAOMR001Impl] - %s";

	@Override
	public List<Map<String, Object>> executeGetVersionInformation() {
		List<Map<String, Object>> mapOut = null;

		try {
			LOGGER.info(String.format(TRACE_PAOMR001_IMPL, "executeGetVersionInformation START"));
			mapOut = this.jdbcUtils.queryForList("PAOM.readVersionData");
		} catch (NoResultException e) {
			LOGGER.error(String.format(TRACE_PAOMR001_IMPL, " Error executeGetVersionInformation: " + e.getMessage()));
			this.addAdvice(ERROR_GET_BASIC_DATA);
			return Collections.emptyList();
		}
		LOGGER.info(String.format(TRACE_PAOMR001_IMPL, "executeGetVersionInformation END"));
		return mapOut;
	}
}
