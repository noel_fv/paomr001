package com.bbva.paom.lib.r001;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.apx.exception.db.NoResultException;
import com.bbva.elara.domain.transaction.Context;
import com.bbva.elara.domain.transaction.ThreadContext;
import com.bbva.elara.utility.jdbc.connector.JdbcUtils;
import com.bbva.paom.lib.r001.impl.PAOMR001Impl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/META-INF/spring/PAOMR001-app.xml",
		"classpath:/META-INF/spring/PAOMR001-app-test.xml", "classpath:/META-INF/spring/PAOMR001-arc.xml",
		"classpath:/META-INF/spring/PAOMR001-arc-test.xml" })
public class PAOMR001Test {

	private PAOMR001 paomR001WithData;
	private PAOMR001 paomR001WithNoData;
	private PAOMR001 paomR001WithException;

	@Spy
	private Context context = new Context();

	@Before
	public void setUp() throws Exception {
		context.setAdviceList(new LinkedList<>());
		ThreadContext.set(context);

		JdbcUtils jdbcUtilsWithData = Mockito.mock(JdbcUtils.class);
		Mockito.when(jdbcUtilsWithData.queryForList("PAOM.readVersionData")).thenReturn(setOutput());

		JdbcUtils jdbcUtilsWithNoData = Mockito.mock(JdbcUtils.class);
		Mockito.when(jdbcUtilsWithNoData.queryForList("PAOM.readVersionData")).thenReturn(null);

		JdbcUtils jdbcUtilsWithException = Mockito.mock(JdbcUtils.class);
		Mockito.when(jdbcUtilsWithException.queryForList("PAOM.readVersionData"))
				.thenThrow(new NoResultException("Error message"));

		this.paomR001WithData = new PAOMR001Impl() {
			@Override
			public void setJdbcUtils(com.bbva.elara.utility.jdbc.JdbcUtils jdbcUtils) {
				super.setJdbcUtils(jdbcUtilsWithData);
			}
		};

		this.paomR001WithNoData = new PAOMR001Impl() {
			@Override
			public void setJdbcUtils(com.bbva.elara.utility.jdbc.JdbcUtils jdbcUtils) {
				super.setJdbcUtils(jdbcUtilsWithNoData);
			}
		};

		this.paomR001WithException = new PAOMR001Impl() {
			@Override
			public void setJdbcUtils(com.bbva.elara.utility.jdbc.JdbcUtils jdbcUtils) {
				super.setJdbcUtils(jdbcUtilsWithException);
			}
		};

		((PAOMR001Impl) paomR001WithData).setJdbcUtils(jdbcUtilsWithData);
		((PAOMR001Impl) paomR001WithNoData).setJdbcUtils(jdbcUtilsWithNoData);
		((PAOMR001Impl) paomR001WithException).setJdbcUtils(jdbcUtilsWithException);

	}

	@Test
	public void executeGetData() {
		assertNotNull(paomR001WithData.executeGetVersionInformation());
	}

	@Test
	public void executeGetNoData() {
		assertNull(paomR001WithNoData.executeGetVersionInformation());
	}

	@Test
	public void executeGetNoResultException() {
		assertTrue(paomR001WithException.executeGetVersionInformation().isEmpty());
	}

	private List<Map<String, Object>> setOutput() {
		List<Map<String, Object>> mapOut = new ArrayList<Map<String, Object>>();
		Map<String, Object> output = new HashMap<String, Object>();
		output.put("NOMBRE_RULES", "valor1");
		output.put("VERSION", "1");
		output.put("FECHA_INICIO", "2020-02-02");
		output.put("FECHA_FIN", "2020-02-08");
		mapOut.add(output);
		return mapOut;
	}

}
